﻿Public Class Form1
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If verifyPassword() Then
            MessageBox.Show("Logged In")
        Else
            MessageBox.Show("Sorry, you have exceeded 5 tries")
        End If
    End Sub

    Private Function verifyPassword() As Boolean
        Dim pass As String = ""
        Dim count As Integer = 5
        Do
            count -= 1
            pass = InputBox("What is the password?").ToUpper
        Loop While pass <> "PASSWORD" And count > 0
        Return pass = "PASSWORD" And count > 0
    End Function
End Class
