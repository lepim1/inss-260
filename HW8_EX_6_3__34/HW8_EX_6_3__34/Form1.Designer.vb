﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lstBoxStates = New System.Windows.Forms.ListBox()
        Me.lstOriginal = New System.Windows.Forms.ListBox()
        Me.btOriginal = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lstBoxStates
        '
        Me.lstBoxStates.FormattingEnabled = True
        Me.lstBoxStates.Location = New System.Drawing.Point(12, 12)
        Me.lstBoxStates.Name = "lstBoxStates"
        Me.lstBoxStates.Size = New System.Drawing.Size(120, 95)
        Me.lstBoxStates.TabIndex = 0
        '
        'lstOriginal
        '
        Me.lstOriginal.FormattingEnabled = True
        Me.lstOriginal.Location = New System.Drawing.Point(138, 12)
        Me.lstOriginal.Name = "lstOriginal"
        Me.lstOriginal.Size = New System.Drawing.Size(120, 134)
        Me.lstOriginal.TabIndex = 1
        '
        'btOriginal
        '
        Me.btOriginal.Location = New System.Drawing.Point(12, 113)
        Me.btOriginal.Name = "btOriginal"
        Me.btOriginal.Size = New System.Drawing.Size(120, 33)
        Me.btOriginal.TabIndex = 2
        Me.btOriginal.Text = "Display Original"
        Me.btOriginal.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(270, 160)
        Me.Controls.Add(Me.btOriginal)
        Me.Controls.Add(Me.lstOriginal)
        Me.Controls.Add(Me.lstBoxStates)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lstBoxStates As System.Windows.Forms.ListBox
    Friend WithEvents lstOriginal As System.Windows.Forms.ListBox
    Friend WithEvents btOriginal As System.Windows.Forms.Button

End Class
