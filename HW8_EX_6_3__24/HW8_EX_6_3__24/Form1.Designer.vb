﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lstBoxStates = New System.Windows.Forms.ListBox()
        Me.btBeginWithNew = New System.Windows.Forms.Button()
        Me.lstBoxStatesWithNew = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'lstBoxStates
        '
        Me.lstBoxStates.FormattingEnabled = True
        Me.lstBoxStates.Location = New System.Drawing.Point(12, 12)
        Me.lstBoxStates.Name = "lstBoxStates"
        Me.lstBoxStates.Size = New System.Drawing.Size(106, 147)
        Me.lstBoxStates.TabIndex = 0
        '
        'btBeginWithNew
        '
        Me.btBeginWithNew.BackColor = System.Drawing.Color.DodgerBlue
        Me.btBeginWithNew.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btBeginWithNew.ForeColor = System.Drawing.Color.White
        Me.btBeginWithNew.Location = New System.Drawing.Point(124, 12)
        Me.btBeginWithNew.Name = "btBeginWithNew"
        Me.btBeginWithNew.Size = New System.Drawing.Size(100, 72)
        Me.btBeginWithNew.TabIndex = 1
        Me.btBeginWithNew.Text = "Display States Whose Name Begins with New"
        Me.btBeginWithNew.UseVisualStyleBackColor = False
        '
        'lstBoxStatesWithNew
        '
        Me.lstBoxStatesWithNew.FormattingEnabled = True
        Me.lstBoxStatesWithNew.Location = New System.Drawing.Point(124, 90)
        Me.lstBoxStatesWithNew.Name = "lstBoxStatesWithNew"
        Me.lstBoxStatesWithNew.Size = New System.Drawing.Size(100, 69)
        Me.lstBoxStatesWithNew.TabIndex = 2
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(231, 162)
        Me.Controls.Add(Me.lstBoxStatesWithNew)
        Me.Controls.Add(Me.btBeginWithNew)
        Me.Controls.Add(Me.lstBoxStates)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lstBoxStates As System.Windows.Forms.ListBox
    Friend WithEvents btBeginWithNew As System.Windows.Forms.Button
    Friend WithEvents lstBoxStatesWithNew As System.Windows.Forms.ListBox

End Class
