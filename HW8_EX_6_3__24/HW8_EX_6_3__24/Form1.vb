﻿Public Class Form1
    Private strStates() As String
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        strStates = IO.File.ReadAllLines("\\vmware-host\Shared Folders\Documents\Visual Studio 2010\Projects\States.txt")
        For Each state As String In strStates
            lstBoxStates.Items.Add(state)
        Next
    End Sub

    Private Sub btBeginWithNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btBeginWithNew.Click
        Dim query = From state In strStates
                    Where state.StartsWith("New")
                    Select state
        For Each state As String In query
            lstBoxStatesWithNew.Items.Add(state)
        Next
    End Sub
End Class
