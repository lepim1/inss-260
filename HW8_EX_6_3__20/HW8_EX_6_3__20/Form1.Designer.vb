﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btReverse = New System.Windows.Forms.Button()
        Me.lstBoxAscStates = New System.Windows.Forms.ListBox()
        Me.lstBoxDescStates = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'btReverse
        '
        Me.btReverse.Location = New System.Drawing.Point(3, 156)
        Me.btReverse.Name = "btReverse"
        Me.btReverse.Size = New System.Drawing.Size(120, 23)
        Me.btReverse.TabIndex = 0
        Me.btReverse.Text = "Rev Alphabetize"
        Me.btReverse.UseVisualStyleBackColor = True
        '
        'lstBoxAscStates
        '
        Me.lstBoxAscStates.FormattingEnabled = True
        Me.lstBoxAscStates.Location = New System.Drawing.Point(3, 3)
        Me.lstBoxAscStates.Name = "lstBoxAscStates"
        Me.lstBoxAscStates.Size = New System.Drawing.Size(120, 147)
        Me.lstBoxAscStates.TabIndex = 1
        '
        'lstBoxDescStates
        '
        Me.lstBoxDescStates.FormattingEnabled = True
        Me.lstBoxDescStates.Location = New System.Drawing.Point(129, 3)
        Me.lstBoxDescStates.Name = "lstBoxDescStates"
        Me.lstBoxDescStates.Size = New System.Drawing.Size(120, 173)
        Me.lstBoxDescStates.TabIndex = 2
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(255, 182)
        Me.Controls.Add(Me.lstBoxDescStates)
        Me.Controls.Add(Me.lstBoxAscStates)
        Me.Controls.Add(Me.btReverse)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btReverse As System.Windows.Forms.Button
    Friend WithEvents lstBoxAscStates As System.Windows.Forms.ListBox
    Friend WithEvents lstBoxDescStates As System.Windows.Forms.ListBox

End Class
