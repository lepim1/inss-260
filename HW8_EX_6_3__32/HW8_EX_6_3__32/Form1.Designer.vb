﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lstBoxStates = New System.Windows.Forms.ListBox()
        Me.btLastState = New System.Windows.Forms.Button()
        Me.txtLastState = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'lstBoxStates
        '
        Me.lstBoxStates.FormattingEnabled = True
        Me.lstBoxStates.Location = New System.Drawing.Point(12, 12)
        Me.lstBoxStates.Name = "lstBoxStates"
        Me.lstBoxStates.Size = New System.Drawing.Size(102, 134)
        Me.lstBoxStates.TabIndex = 0
        '
        'btLastState
        '
        Me.btLastState.Location = New System.Drawing.Point(120, 12)
        Me.btLastState.Name = "btLastState"
        Me.btLastState.Size = New System.Drawing.Size(152, 63)
        Me.btLastState.TabIndex = 1
        Me.btLastState.Text = "Display Name of Last State"
        Me.btLastState.UseVisualStyleBackColor = True
        '
        'txtLastState
        '
        Me.txtLastState.Enabled = False
        Me.txtLastState.Location = New System.Drawing.Point(120, 103)
        Me.txtLastState.Name = "txtLastState"
        Me.txtLastState.Size = New System.Drawing.Size(152, 20)
        Me.txtLastState.TabIndex = 2
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 155)
        Me.Controls.Add(Me.txtLastState)
        Me.Controls.Add(Me.btLastState)
        Me.Controls.Add(Me.lstBoxStates)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lstBoxStates As System.Windows.Forms.ListBox
    Friend WithEvents btLastState As System.Windows.Forms.Button
    Friend WithEvents txtLastState As System.Windows.Forms.TextBox

End Class
