﻿Public Class Form1
    Private strStates() As String
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        strStates = IO.File.ReadAllLines("\\vmware-host\Shared Folders\Documents\Visual Studio 2010\Projects\States.txt")
        For Each state As String In strStates
            lstBoxStates.Items.Add(state)
        Next
    End Sub

    Private Sub btLastState_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btLastState.Click
        txtLastState.Text = lstBoxStates.Items(lstBoxStates.Items.Count - 1)
    End Sub
End Class
