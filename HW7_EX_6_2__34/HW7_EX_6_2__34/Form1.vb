﻿Public Class Form1
    Private Sub btGetFiveGrades_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btGetFiveGrades.Click
        Dim sum As Double = 0
        Try
            For i As Integer = 1 To 5
                sum += CDbl(InputBox("Enter the grade number " & i).Replace(".", ","))
            Next
        Catch ex As Exception
            MessageBox.Show("Please make sure you have entered only numbers")
        End Try
        txtAverage.Text = CStr(sum / 5)
    End Sub
End Class
