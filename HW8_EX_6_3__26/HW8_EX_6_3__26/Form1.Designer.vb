﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lstStates = New System.Windows.Forms.ListBox()
        Me.btCalcShortest = New System.Windows.Forms.Button()
        Me.lstShortest = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'lstStates
        '
        Me.lstStates.FormattingEnabled = True
        Me.lstStates.Location = New System.Drawing.Point(12, 12)
        Me.lstStates.Name = "lstStates"
        Me.lstStates.Size = New System.Drawing.Size(95, 95)
        Me.lstStates.TabIndex = 0
        '
        'btCalcShortest
        '
        Me.btCalcShortest.Location = New System.Drawing.Point(113, 12)
        Me.btCalcShortest.Name = "btCalcShortest"
        Me.btCalcShortest.Size = New System.Drawing.Size(106, 46)
        Me.btCalcShortest.TabIndex = 1
        Me.btCalcShortest.Text = "Display States with Shortest Names"
        Me.btCalcShortest.UseVisualStyleBackColor = True
        '
        'lstShortest
        '
        Me.lstShortest.FormattingEnabled = True
        Me.lstShortest.Location = New System.Drawing.Point(113, 64)
        Me.lstShortest.Name = "lstShortest"
        Me.lstShortest.Size = New System.Drawing.Size(106, 43)
        Me.lstShortest.TabIndex = 2
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(231, 117)
        Me.Controls.Add(Me.lstShortest)
        Me.Controls.Add(Me.btCalcShortest)
        Me.Controls.Add(Me.lstStates)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lstStates As System.Windows.Forms.ListBox
    Friend WithEvents btCalcShortest As System.Windows.Forms.Button
    Friend WithEvents lstShortest As System.Windows.Forms.ListBox

End Class
