﻿Public Class Form1
    Private strStates() As String
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        strStates = IO.File.ReadAllLines("\\vmware-host\Shared Folders\Documents\Visual Studio 2010\Projects\States.txt")
        For Each state As String In strStates
            lstStates.Items.Add(state)
        Next
    End Sub

    Private Sub btCalcShortest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btCalcShortest.Click
        Dim shortestLength As Integer = strStates(0).Length
        For i As Integer = 1 To strStates.Length - 1
            If strStates(i).Length < shortestLength Then
                shortestLength = strStates(i).Length
            End If
        Next

        For Each state As String In strStates
            If state.Length = shortestLength Then
                lstShortest.Items.Add(state)
            End If
        Next

        'or
        'Dim query = From state In strStates
        'Where(state.Length = shortestLength)
        '          Select state
        'For Each state As String In query
        'lstShortest.Items.Add(state)
        'Next

    End Sub
End Class
