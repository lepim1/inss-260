﻿Public Class Form1
    Private strStates() As String
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        strStates = IO.File.ReadAllLines("\\vmware-host\Shared Folders\Documents\Visual Studio 2010\Projects\States.txt")
        For Each state As String In strStates
            lstBoxStates.Items.Add(state)
        Next
    End Sub

    Private Sub lstBoxStates_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstBoxStates.SelectedIndexChanged
        txtNumVowel.Text = CStr(countVoewels(lstBoxStates.SelectedItem.ToString()))
    End Sub

    Private Function countVoewels(ByVal state As String) As Integer
        Dim numVowels As Integer = 0
        state = state.ToUpper
        For i As Integer = 0 To state.Length - 1
            If (state(i) = "A" Or state(i) = "E" Or state(i) = "I" Or state(i) = "O" Or state(i) = "U") Then
                numVowels += 1
            End If
        Next
        Return numVowels
    End Function

End Class
