﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lstBoxStates = New System.Windows.Forms.ListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtNumVowel = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'lstBoxStates
        '
        Me.lstBoxStates.FormattingEnabled = True
        Me.lstBoxStates.Location = New System.Drawing.Point(12, 12)
        Me.lstBoxStates.Name = "lstBoxStates"
        Me.lstBoxStates.Size = New System.Drawing.Size(120, 238)
        Me.lstBoxStates.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(161, 93)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(92, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Number of vowels"
        '
        'txtNumVowel
        '
        Me.txtNumVowel.Enabled = False
        Me.txtNumVowel.Location = New System.Drawing.Point(176, 109)
        Me.txtNumVowel.Name = "txtNumVowel"
        Me.txtNumVowel.Size = New System.Drawing.Size(64, 20)
        Me.txtNumVowel.TabIndex = 2
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.txtNumVowel)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lstBoxStates)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lstBoxStates As System.Windows.Forms.ListBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtNumVowel As System.Windows.Forms.TextBox

End Class
