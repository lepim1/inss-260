﻿Public Class Form1
    Const SUCCESS As Integer = 1
    Const ERROR_COEFFICIENT As Integer = 2
    Const ERROR_INITIAL_HEIGHT As Integer = 3
    Const ERROR_EXCEPTION As Integer = 4

    Private msgStatus As String
    Private status As Integer

    Private Sub btAnalyze_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAnalyze.Click
        Dim coefficient, initialHeight As Double
        Dim nBallBounces As Integer
        Dim metersTraveled As Double
        Try
            input(coefficient, initialHeight)
            validateFields(coefficient, initialHeight)
            If (status = SUCCESS) Then
                metersTraveled = process(coefficient, initialHeight, nBallBounces)
                output(metersTraveled, nBallBounces)
            Else
                outputError()
            End If
        Catch ex As Exception
            status = ERROR_EXCEPTION
            outputError()
        End Try
    End Sub

    Public Sub input(ByRef coefficient As Double, ByRef initialHeight As Double)
        status = SUCCESS
        msgStatus = ""
        coefficient = CDbl(txtCoefficient.Text.Replace(".", ","))
        initialHeight = CDbl(txtInitialHeight.Text)
    End Sub

    Public Sub validateFields(ByVal coefficient As Double, ByVal initialHeight As Double)
        validateCoefficient(coefficient)
        If status <> SUCCESS Then
            Return
        End If
        validateInitialHeight(initialHeight)
        If status <> SUCCESS Then
            Return
        End If
    End Sub

    Public Sub validateCoefficient(ByVal coefficient As Double)
        If (coefficient <= 0 Or coefficient >= 1) Then
            status = ERROR_COEFFICIENT
        End If
    End Sub

    Public Sub validateInitialHeight(ByVal initialHeight As Double)
        If (initialHeight <= 0) Then
            status = ERROR_INITIAL_HEIGHT
        End If
    End Sub

    Public Function process(ByVal coefficient As Double, ByVal height As Double, ByRef nBallBounces As Double) As Double
        Dim metersTraveled As Double = 0
        nBallBounces = 0
        Do
            metersTraveled += height
            nBallBounces += 1
            height = coefficient * height
        Loop While height >= 0.1
        Return metersTraveled
    End Function

    Public Function convertMetersToCentimeters(ByVal initialHeight As Double) As Double
        Return initialHeight * 100
    End Function

    Private Sub output(ByVal metersTraveled As Double, ByVal nBallBounces As Integer)
        txtMetersTraveled.Text = CStr(metersTraveled)
        txtBounces.Text = CStr(nBallBounces)
    End Sub

    Private Sub outputError()
        Select Case status
            Case ERROR_COEFFICIENT
                msgStatus = "Coefficient should be a number between 0 and 1 (Ex.: 0,1)"
            Case ERROR_EXCEPTION
                msgStatus = "Error: make sure that the coefficient is a decimal number and the high is a number"
            Case ERROR_INITIAL_HEIGHT
                msgStatus = "The initial height should be a number greater than 0"
            Case Else
                msgStatus = "We apologize for the inconvenience but something went wrong"
        End Select
        MessageBox.Show(msgStatus)
    End Sub

End Class
