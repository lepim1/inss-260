﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtCoefficient = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtInitialHeight = New System.Windows.Forms.TextBox()
        Me.btAnalyze = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtBounces = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtMetersTraveled = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(11, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(117, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Coefficient of restitution"
        '
        'txtCoefficient
        '
        Me.txtCoefficient.Location = New System.Drawing.Point(152, 5)
        Me.txtCoefficient.Name = "txtCoefficient"
        Me.txtCoefficient.Size = New System.Drawing.Size(100, 20)
        Me.txtCoefficient.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(17, 35)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(108, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Initial height in meters"
        '
        'txtInitialHeight
        '
        Me.txtInitialHeight.Location = New System.Drawing.Point(152, 32)
        Me.txtInitialHeight.Name = "txtInitialHeight"
        Me.txtInitialHeight.Size = New System.Drawing.Size(100, 20)
        Me.txtInitialHeight.TabIndex = 3
        '
        'btAnalyze
        '
        Me.btAnalyze.Location = New System.Drawing.Point(14, 58)
        Me.btAnalyze.Name = "btAnalyze"
        Me.btAnalyze.Size = New System.Drawing.Size(238, 38)
        Me.btAnalyze.TabIndex = 4
        Me.btAnalyze.Text = "Analyze Bouncing Ball"
        Me.btAnalyze.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(28, 105)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Number of bounces"
        '
        'txtBounces
        '
        Me.txtBounces.Enabled = False
        Me.txtBounces.Location = New System.Drawing.Point(152, 102)
        Me.txtBounces.Name = "txtBounces"
        Me.txtBounces.Size = New System.Drawing.Size(100, 20)
        Me.txtBounces.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(48, 127)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Meters traveled"
        '
        'txtMetersTraveled
        '
        Me.txtMetersTraveled.Enabled = False
        Me.txtMetersTraveled.Location = New System.Drawing.Point(152, 124)
        Me.txtMetersTraveled.Name = "txtMetersTraveled"
        Me.txtMetersTraveled.Size = New System.Drawing.Size(100, 20)
        Me.txtMetersTraveled.TabIndex = 8
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(264, 171)
        Me.Controls.Add(Me.txtMetersTraveled)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtBounces)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btAnalyze)
        Me.Controls.Add(Me.txtInitialHeight)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtCoefficient)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.Text = "Bouncing Ball"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtCoefficient As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtInitialHeight As System.Windows.Forms.TextBox
    Friend WithEvents btAnalyze As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtBounces As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtMetersTraveled As System.Windows.Forms.TextBox

End Class
