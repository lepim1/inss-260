﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btDisplay = New System.Windows.Forms.Button()
        Me.lstMoney = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'btDisplay
        '
        Me.btDisplay.Location = New System.Drawing.Point(47, 12)
        Me.btDisplay.Name = "btDisplay"
        Me.btDisplay.Size = New System.Drawing.Size(215, 32)
        Me.btDisplay.TabIndex = 1
        Me.btDisplay.Text = "Display Growth of Money"
        Me.btDisplay.UseVisualStyleBackColor = True
        '
        'lstMoney
        '
        Me.lstMoney.FormattingEnabled = True
        Me.lstMoney.Location = New System.Drawing.Point(47, 79)
        Me.lstMoney.MultiColumn = True
        Me.lstMoney.Name = "lstMoney"
        Me.lstMoney.Size = New System.Drawing.Size(215, 147)
        Me.lstMoney.TabIndex = 2
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(314, 302)
        Me.Controls.Add(Me.lstMoney)
        Me.Controls.Add(Me.btDisplay)
        Me.Name = "Form1"
        Me.Text = "Interest"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btDisplay As System.Windows.Forms.Button
    Friend WithEvents lstMoney As System.Windows.Forms.ListBox

End Class
