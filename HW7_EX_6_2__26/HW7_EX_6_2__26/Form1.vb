﻿Public Class Form1

    Private Sub btDisplay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btDisplay.Click
        lstMoney.ColumnWidth = 70
        lstMoney.Items.Add("")
        lstMoney.Items.Add("YEAR")
        For i As Integer = 1 To 9 Step 1
            lstMoney.Items.Add(i)
        Next


        Dim money As Double = 1000

        lstMoney.Items.Add("SIMPLE")
        lstMoney.Items.Add("INTEREST")

        For i As Integer = 1 To 9 Step 1
            lstMoney.Items.Add((money + (0.05 * money * i)).ToString("C"))
        Next


        lstMoney.Items.Add("COMPOUND")
        lstMoney.Items.Add("INTEREST")
        For i As Integer = 1 To 9 Step 1
            money = money * 1.05
            lstMoney.Items.Add(money.ToString("C"))
        Next

    End Sub
End Class
