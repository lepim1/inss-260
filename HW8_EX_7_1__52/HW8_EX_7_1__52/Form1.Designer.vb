﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btDisplayStates = New System.Windows.Forms.Button()
        Me.lstBoxStates = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'btDisplayStates
        '
        Me.btDisplayStates.Location = New System.Drawing.Point(59, 12)
        Me.btDisplayStates.Name = "btDisplayStates"
        Me.btDisplayStates.Size = New System.Drawing.Size(177, 23)
        Me.btDisplayStates.TabIndex = 0
        Me.btDisplayStates.Text = "Display States Begining with New"
        Me.btDisplayStates.UseVisualStyleBackColor = True
        '
        'lstBoxStates
        '
        Me.lstBoxStates.FormattingEnabled = True
        Me.lstBoxStates.Location = New System.Drawing.Point(59, 41)
        Me.lstBoxStates.Name = "lstBoxStates"
        Me.lstBoxStates.Size = New System.Drawing.Size(177, 95)
        Me.lstBoxStates.TabIndex = 1
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 146)
        Me.Controls.Add(Me.lstBoxStates)
        Me.Controls.Add(Me.btDisplayStates)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btDisplayStates As System.Windows.Forms.Button
    Friend WithEvents lstBoxStates As System.Windows.Forms.ListBox

End Class
