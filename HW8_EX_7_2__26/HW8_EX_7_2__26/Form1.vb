﻿Public Class Form1

    Private Sub btSuperBowlWinners_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btSuperBowlWinners.Click
        Dim strWinners = IO.File.ReadAllLines("\\vmware-host\Shared Folders\Documents\Visual Studio 2010\Projects\SBWinners.txt")
        Dim sumWinners = From winner In strWinners
                             Group By winner
                             Into Sum(1)
                             Order By winner
                             Order By Sum Descending
        For Each winner As Object In sumWinners
            lstWinners.Items.Add(winner.Sum & " " & winner.winner)
        Next
    End Sub
End Class
