﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btSuperBowlWinners = New System.Windows.Forms.Button()
        Me.lstWinners = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'btSuperBowlWinners
        '
        Me.btSuperBowlWinners.Location = New System.Drawing.Point(66, 25)
        Me.btSuperBowlWinners.Name = "btSuperBowlWinners"
        Me.btSuperBowlWinners.Size = New System.Drawing.Size(164, 51)
        Me.btSuperBowlWinners.TabIndex = 0
        Me.btSuperBowlWinners.Text = "Display Super Bowl Winners"
        Me.btSuperBowlWinners.UseVisualStyleBackColor = True
        '
        'lstWinners
        '
        Me.lstWinners.FormattingEnabled = True
        Me.lstWinners.Location = New System.Drawing.Point(66, 82)
        Me.lstWinners.Name = "lstWinners"
        Me.lstWinners.Size = New System.Drawing.Size(164, 108)
        Me.lstWinners.TabIndex = 1
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.lstWinners)
        Me.Controls.Add(Me.btSuperBowlWinners)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btSuperBowlWinners As System.Windows.Forms.Button
    Friend WithEvents lstWinners As System.Windows.Forms.ListBox

End Class
