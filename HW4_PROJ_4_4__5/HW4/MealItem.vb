﻿Public Class MealItem
    Private Property _price As Double

    Protected Property Price() As Double
        Get
            Return _price
        End Get
        Set(ByVal value As Double)
            _price = value
        End Set
    End Property

    Public Overridable Function calcTotal() As Double
        Return _price
    End Function

End Class
