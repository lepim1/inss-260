﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbBurgers = New System.Windows.Forms.CheckBox()
        Me.cbFries = New System.Windows.Forms.CheckBox()
        Me.cbDrinks = New System.Windows.Forms.CheckBox()
        Me.gbBurgers = New System.Windows.Forms.GroupBox()
        Me.rbBurgerBaconCheese = New System.Windows.Forms.RadioButton()
        Me.rbBurgerBacon = New System.Windows.Forms.RadioButton()
        Me.rbBurgerCheese = New System.Windows.Forms.RadioButton()
        Me.rbBurgerRegular = New System.Windows.Forms.RadioButton()
        Me.gbFries = New System.Windows.Forms.GroupBox()
        Me.rbFriesLarge = New System.Windows.Forms.RadioButton()
        Me.rbFriesMedium = New System.Windows.Forms.RadioButton()
        Me.rbFriesSmall = New System.Windows.Forms.RadioButton()
        Me.gbDrinks = New System.Windows.Forms.GroupBox()
        Me.rbBottledWater = New System.Windows.Forms.RadioButton()
        Me.rbSoda = New System.Windows.Forms.RadioButton()
        Me.btnComputeTotal = New System.Windows.Forms.Button()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.gbBurgers.SuspendLayout()
        Me.gbFries.SuspendLayout()
        Me.gbDrinks.SuspendLayout()
        Me.SuspendLayout()
        '
        'cbBurgers
        '
        Me.cbBurgers.AutoSize = True
        Me.cbBurgers.Location = New System.Drawing.Point(38, 42)
        Me.cbBurgers.Name = "cbBurgers"
        Me.cbBurgers.Size = New System.Drawing.Size(62, 17)
        Me.cbBurgers.TabIndex = 0
        Me.cbBurgers.Text = "Burgers"
        Me.cbBurgers.UseVisualStyleBackColor = True
        '
        'cbFries
        '
        Me.cbFries.AutoSize = True
        Me.cbFries.Location = New System.Drawing.Point(38, 173)
        Me.cbFries.Name = "cbFries"
        Me.cbFries.Size = New System.Drawing.Size(48, 17)
        Me.cbFries.TabIndex = 1
        Me.cbFries.Text = "Fries"
        Me.cbFries.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.cbFries.UseVisualStyleBackColor = True
        '
        'cbDrinks
        '
        Me.cbDrinks.AutoSize = True
        Me.cbDrinks.Location = New System.Drawing.Point(38, 280)
        Me.cbDrinks.Name = "cbDrinks"
        Me.cbDrinks.Size = New System.Drawing.Size(56, 17)
        Me.cbDrinks.TabIndex = 2
        Me.cbDrinks.Text = "Drinks"
        Me.cbDrinks.UseVisualStyleBackColor = True
        '
        'gbBurgers
        '
        Me.gbBurgers.Controls.Add(Me.rbBurgerBaconCheese)
        Me.gbBurgers.Controls.Add(Me.rbBurgerBacon)
        Me.gbBurgers.Controls.Add(Me.rbBurgerCheese)
        Me.gbBurgers.Controls.Add(Me.rbBurgerRegular)
        Me.gbBurgers.Location = New System.Drawing.Point(121, 42)
        Me.gbBurgers.Name = "gbBurgers"
        Me.gbBurgers.Size = New System.Drawing.Size(200, 118)
        Me.gbBurgers.TabIndex = 3
        Me.gbBurgers.TabStop = False
        Me.gbBurgers.Text = "Choices for Burgers"
        Me.gbBurgers.Visible = False
        '
        'rbBurgerBaconCheese
        '
        Me.rbBurgerBaconCheese.AutoSize = True
        Me.rbBurgerBaconCheese.Location = New System.Drawing.Point(20, 88)
        Me.rbBurgerBaconCheese.Name = "rbBurgerBaconCheese"
        Me.rbBurgerBaconCheese.Size = New System.Drawing.Size(160, 17)
        Me.rbBurgerBaconCheese.TabIndex = 3
        Me.rbBurgerBaconCheese.TabStop = True
        Me.rbBurgerBaconCheese.Text = "w/ bacon and cheese (5.39)"
        Me.rbBurgerBaconCheese.UseVisualStyleBackColor = True
        '
        'rbBurgerBacon
        '
        Me.rbBurgerBacon.AutoSize = True
        Me.rbBurgerBacon.Location = New System.Drawing.Point(20, 65)
        Me.rbBurgerBacon.Name = "rbBurgerBacon"
        Me.rbBurgerBacon.Size = New System.Drawing.Size(101, 17)
        Me.rbBurgerBacon.TabIndex = 2
        Me.rbBurgerBacon.TabStop = True
        Me.rbBurgerBacon.Text = "w/ bacon (4.79)"
        Me.rbBurgerBacon.UseVisualStyleBackColor = True
        '
        'rbBurgerCheese
        '
        Me.rbBurgerCheese.AutoSize = True
        Me.rbBurgerCheese.Location = New System.Drawing.Point(20, 42)
        Me.rbBurgerCheese.Name = "rbBurgerCheese"
        Me.rbBurgerCheese.Size = New System.Drawing.Size(106, 17)
        Me.rbBurgerCheese.TabIndex = 1
        Me.rbBurgerCheese.TabStop = True
        Me.rbBurgerCheese.Text = "w/ cheese (4.79)"
        Me.rbBurgerCheese.UseVisualStyleBackColor = True
        '
        'rbBurgerRegular
        '
        Me.rbBurgerRegular.AutoSize = True
        Me.rbBurgerRegular.Location = New System.Drawing.Point(20, 19)
        Me.rbBurgerRegular.Name = "rbBurgerRegular"
        Me.rbBurgerRegular.Size = New System.Drawing.Size(92, 17)
        Me.rbBurgerRegular.TabIndex = 0
        Me.rbBurgerRegular.TabStop = True
        Me.rbBurgerRegular.Text = "Regular (4.19)"
        Me.rbBurgerRegular.UseVisualStyleBackColor = True
        '
        'gbFries
        '
        Me.gbFries.Controls.Add(Me.rbFriesLarge)
        Me.gbFries.Controls.Add(Me.rbFriesMedium)
        Me.gbFries.Controls.Add(Me.rbFriesSmall)
        Me.gbFries.Location = New System.Drawing.Point(121, 166)
        Me.gbFries.Name = "gbFries"
        Me.gbFries.Size = New System.Drawing.Size(200, 100)
        Me.gbFries.TabIndex = 4
        Me.gbFries.TabStop = False
        Me.gbFries.Text = "Choices for Fries"
        Me.gbFries.Visible = False
        '
        'rbFriesLarge
        '
        Me.rbFriesLarge.AutoSize = True
        Me.rbFriesLarge.Location = New System.Drawing.Point(20, 65)
        Me.rbFriesLarge.Name = "rbFriesLarge"
        Me.rbFriesLarge.Size = New System.Drawing.Size(82, 17)
        Me.rbFriesLarge.TabIndex = 2
        Me.rbFriesLarge.TabStop = True
        Me.rbFriesLarge.Text = "Large (4.99)"
        Me.rbFriesLarge.UseVisualStyleBackColor = True
        '
        'rbFriesMedium
        '
        Me.rbFriesMedium.AutoSize = True
        Me.rbFriesMedium.Location = New System.Drawing.Point(20, 42)
        Me.rbFriesMedium.Name = "rbFriesMedium"
        Me.rbFriesMedium.Size = New System.Drawing.Size(92, 17)
        Me.rbFriesMedium.TabIndex = 1
        Me.rbFriesMedium.TabStop = True
        Me.rbFriesMedium.Text = "Medium (3.09)"
        Me.rbFriesMedium.UseVisualStyleBackColor = True
        '
        'rbFriesSmall
        '
        Me.rbFriesSmall.AutoSize = True
        Me.rbFriesSmall.Location = New System.Drawing.Point(20, 19)
        Me.rbFriesSmall.Name = "rbFriesSmall"
        Me.rbFriesSmall.Size = New System.Drawing.Size(80, 17)
        Me.rbFriesSmall.TabIndex = 0
        Me.rbFriesSmall.TabStop = True
        Me.rbFriesSmall.Text = "Small (2.39)"
        Me.rbFriesSmall.UseVisualStyleBackColor = True
        '
        'gbDrinks
        '
        Me.gbDrinks.Controls.Add(Me.rbBottledWater)
        Me.gbDrinks.Controls.Add(Me.rbSoda)
        Me.gbDrinks.Location = New System.Drawing.Point(121, 280)
        Me.gbDrinks.Name = "gbDrinks"
        Me.gbDrinks.Size = New System.Drawing.Size(200, 66)
        Me.gbDrinks.TabIndex = 5
        Me.gbDrinks.TabStop = False
        Me.gbDrinks.Text = "Choices for Drinks"
        Me.gbDrinks.Visible = False
        '
        'rbBottledWater
        '
        Me.rbBottledWater.AutoSize = True
        Me.rbBottledWater.Location = New System.Drawing.Point(20, 42)
        Me.rbBottledWater.Name = "rbBottledWater"
        Me.rbBottledWater.Size = New System.Drawing.Size(117, 17)
        Me.rbBottledWater.TabIndex = 1
        Me.rbBottledWater.TabStop = True
        Me.rbBottledWater.Text = "Bottled water (1.49)"
        Me.rbBottledWater.UseVisualStyleBackColor = True
        '
        'rbSoda
        '
        Me.rbSoda.AutoSize = True
        Me.rbSoda.Location = New System.Drawing.Point(20, 19)
        Me.rbSoda.Name = "rbSoda"
        Me.rbSoda.Size = New System.Drawing.Size(80, 17)
        Me.rbSoda.TabIndex = 0
        Me.rbSoda.TabStop = True
        Me.rbSoda.Text = "Soda (1.69)"
        Me.rbSoda.UseVisualStyleBackColor = True
        '
        'btnComputeTotal
        '
        Me.btnComputeTotal.Location = New System.Drawing.Point(348, 281)
        Me.btnComputeTotal.Name = "btnComputeTotal"
        Me.btnComputeTotal.Size = New System.Drawing.Size(166, 35)
        Me.btnComputeTotal.TabIndex = 6
        Me.btnComputeTotal.Text = "Compute Cost of Meal"
        Me.btnComputeTotal.UseVisualStyleBackColor = True
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(345, 326)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(69, 13)
        Me.lblTotal.TabIndex = 7
        Me.lblTotal.Text = "Cost of Meal:"
        '
        'txtTotal
        '
        Me.txtTotal.Location = New System.Drawing.Point(414, 323)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(100, 20)
        Me.txtTotal.TabIndex = 8
        '
        'Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(547, 415)
        Me.Controls.Add(Me.txtTotal)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.btnComputeTotal)
        Me.Controls.Add(Me.gbDrinks)
        Me.Controls.Add(Me.gbFries)
        Me.Controls.Add(Me.gbBurgers)
        Me.Controls.Add(Me.cbDrinks)
        Me.Controls.Add(Me.cbFries)
        Me.Controls.Add(Me.cbBurgers)
        Me.Name = "Form"
        Me.Text = "Sales"
        Me.gbBurgers.ResumeLayout(False)
        Me.gbBurgers.PerformLayout()
        Me.gbFries.ResumeLayout(False)
        Me.gbFries.PerformLayout()
        Me.gbDrinks.ResumeLayout(False)
        Me.gbDrinks.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbBurgers As System.Windows.Forms.CheckBox
    Friend WithEvents cbFries As System.Windows.Forms.CheckBox
    Friend WithEvents cbDrinks As System.Windows.Forms.CheckBox
    Friend WithEvents gbBurgers As System.Windows.Forms.GroupBox
    Friend WithEvents rbBurgerBaconCheese As System.Windows.Forms.RadioButton
    Friend WithEvents rbBurgerBacon As System.Windows.Forms.RadioButton
    Friend WithEvents rbBurgerCheese As System.Windows.Forms.RadioButton
    Friend WithEvents rbBurgerRegular As System.Windows.Forms.RadioButton
    Friend WithEvents gbFries As System.Windows.Forms.GroupBox
    Friend WithEvents gbDrinks As System.Windows.Forms.GroupBox
    Friend WithEvents rbFriesLarge As System.Windows.Forms.RadioButton
    Friend WithEvents rbFriesMedium As System.Windows.Forms.RadioButton
    Friend WithEvents rbFriesSmall As System.Windows.Forms.RadioButton
    Friend WithEvents rbBottledWater As System.Windows.Forms.RadioButton
    Friend WithEvents rbSoda As System.Windows.Forms.RadioButton
    Friend WithEvents btnComputeTotal As System.Windows.Forms.Button
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents txtTotal As System.Windows.Forms.TextBox

End Class
