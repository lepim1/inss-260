﻿Imports System.Globalization 'TO DISPLAY CURRENCY IN EN-US BECAUSE MY SYSTEM IS PT-BR

Public Class Form
    Private Sub btnComputeTotal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnComputeTotal.Click
        Dim items As New List(Of MealItem)
        Dim total As Double = 0
        input(items)
        process(total, items)
        output(total, items)
    End Sub

    Private Sub cbBurgers_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbBurgers.CheckedChanged
        gbBurgers.Visible = cbBurgers.Checked
    End Sub

    Private Sub cbFries_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbFries.CheckedChanged
        gbFries.Visible = cbFries.Checked
    End Sub

    Private Sub cbDrinks_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbDrinks.CheckedChanged
        gbDrinks.Visible = cbDrinks.Checked
    End Sub

    Private Sub input(ByVal items As List(Of MealItem))
        If (cbBurgers.Checked) Then
            If (rbBurgerRegular.Checked) Then
                items.Add(New Burger(Burger.REGULAR))
            End If
            If (rbBurgerCheese.Checked) Then
                items.Add(New Burger(Burger.CHEESE))
            End If
            If (rbBurgerBacon.Checked) Then
                items.Add(New Burger(Burger.BACON))
            End If
            If (rbBurgerBaconCheese.Checked) Then
                items.Add(New Burger(Burger.CHEESE_BACON))
            End If
        End If
        If (cbFries.Checked) Then
            If (rbFriesSmall.Checked) Then
                items.Add(New Fries(Fries.SMALL))
            End If
            If (rbFriesMedium.Checked) Then
                items.Add(New Fries(Fries.MEDIUM))
            End If
            If (rbFriesLarge.Checked) Then
                items.Add(New Fries(Fries.LARGE))
            End If
        End If
        If (cbDrinks.Checked) Then
            If (rbSoda.Checked) Then
                items.Add(New Drink(Drink.SODA))
            End If
            If (rbBottledWater.Checked) Then
                items.Add(New Drink(Drink.BOTTLED_WATER))
            End If
        End If
    End Sub

    Private Sub process(ByRef total As Double, ByVal items As List(Of MealItem))
        For Each item In items
            total += item.calcTotal()
        Next
    End Sub

    Private Sub output(ByVal total As Double, ByVal items As List(Of MealItem))
        txtTotal.Text = total.ToString("C", CultureInfo.CreateSpecificCulture("en-US"))
    End Sub

End Class
