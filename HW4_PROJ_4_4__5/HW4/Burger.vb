﻿Public Class Burger
    Inherits MealItem
    Public Const REGULAR = 0
    Public Const CHEESE = 1
    Public Const BACON = 2
    Public Const CHEESE_BACON = 3

    Sub New(ByVal sort As Integer)
        Select Case sort
            Case REGULAR
                MyBase.Price = 4.19
            Case CHEESE
                MyBase.Price = 4.79
            Case BACON
                MyBase.Price = 4.79
            Case CHEESE_BACON
                MyBase.Price = 5.39
        End Select
    End Sub

    Public Overrides Function calcTotal() As Double
        Return MyBase.calcTotal()
    End Function

End Class
