﻿Public Class Drink
    Inherits MealItem
    Public Const SODA = 0
    Public Const BOTTLED_WATER = 1

    Sub New(ByVal sort As Integer)
        Select Case sort
            Case SODA
                MyBase.Price = 1.69
            Case BOTTLED_WATER
                MyBase.Price = 1.49
        End Select
    End Sub

    Public Overrides Function calcTotal() As Double
        Return MyBase.calcTotal()
    End Function

End Class
