﻿Public Class Fries
    Inherits MealItem
    Public Const SMALL = 0
    Public Const MEDIUM = 1
    Public Const LARGE = 2

    Sub New(ByVal sort As Integer)
        Select Case sort
            Case SMALL
                MyBase.Price = 2.39
            Case MEDIUM
                MyBase.Price = 3.09
            Case LARGE
                MyBase.Price = 4.99
        End Select
    End Sub

    Public Overrides Function calcTotal() As Double
        Return MyBase.calcTotal()
    End Function

End Class
