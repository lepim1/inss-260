﻿Public Class Form1

    Private Sub btSuperBowlWinners_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btSuperBowlWinners.Click
        Dim strWinners = IO.File.ReadAllLines("\\vmware-host\Shared Folders\Documents\Visual Studio 2010\Projects\SBWinners.txt")
        Dim sumWinners = From winner In strWinners
                             Select winner
                             Where winner.StartsWith("B")
                             Distinct
                             Order By winner Ascending
        For Each winner As Object In sumWinners
            lstSuperBowlWinners.Items.Add(winner)
        Next
    End Sub
End Class
