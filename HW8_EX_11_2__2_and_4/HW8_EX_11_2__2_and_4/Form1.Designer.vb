﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btDensity = New System.Windows.Forms.Button()
        Me.dataGridStateDensity = New System.Windows.Forms.DataGridView()
        CType(Me.dataGridStateDensity, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btDensity
        '
        Me.btDensity.Location = New System.Drawing.Point(12, 12)
        Me.btDensity.Name = "btDensity"
        Me.btDensity.Size = New System.Drawing.Size(260, 52)
        Me.btDensity.TabIndex = 0
        Me.btDensity.Text = "Display States and Desisties Ordered by Density in Descending Order"
        Me.btDensity.UseVisualStyleBackColor = True
        '
        'dataGridStateDensity
        '
        Me.dataGridStateDensity.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dataGridStateDensity.Location = New System.Drawing.Point(12, 84)
        Me.dataGridStateDensity.Name = "dataGridStateDensity"
        Me.dataGridStateDensity.Size = New System.Drawing.Size(260, 156)
        Me.dataGridStateDensity.TabIndex = 1
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.dataGridStateDensity)
        Me.Controls.Add(Me.btDensity)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.dataGridStateDensity, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btDensity As System.Windows.Forms.Button
    Friend WithEvents dataGridStateDensity As System.Windows.Forms.DataGridView

End Class
