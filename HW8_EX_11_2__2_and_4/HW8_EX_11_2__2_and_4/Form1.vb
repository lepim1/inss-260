﻿Public Class Form1

    Private Sub btDensity_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btDensity.Click
        Dim lines = IO.File.ReadAllLines("\\vmware-host\Shared Folders\Documents\Visual Studio 2010\Projects\USStates.txt")
        Dim states(lines.Length - 1) As State
        For i As Integer = 0 To lines.Count - 1
            Dim aux() As String = lines(i).Split(","c)
            Dim State As State = New State
            State.Name = aux(0)
            State.Abbreviation = aux(1)
            State.LandArea = CInt(aux(2))
            State.Population = CInt(aux(3))
            states(i) = State
        Next

        Dim query = From state In states
                    Let Density = state.calcDensity()
                    Select state.Name, Density
                    Order By Density Descending

        dataGridStateDensity.DataSource = query.ToList
        dataGridStateDensity.CurrentCell = Nothing
        'dataGridStateDensity.Columns("").HeaderText = ""  
    End Sub
End Class
