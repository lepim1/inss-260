﻿Public Class State
    Private strName As String
    Private strAbbreviation As String
    'Private datEntry As Date
    Private intLandArea As Integer
    Private intPopulation As Integer

    Public Property LandArea() As Integer
        Get
            Return intLandArea
        End Get
        Set(ByVal value As Integer)
            intLandArea = value
        End Set
    End Property

    Public Property Population() As Integer
        Get
            Return intPopulation
        End Get
        Set(ByVal value As Integer)
            intPopulation = value
        End Set
    End Property

    'Public Property DateEntry() As Date
    '    Get
    '        Return datEntry
    '    End Get
    '    Set(ByVal value As Date)
    '        datEntry = value
    '    End Set
    'End Property

    Public Property Abbreviation() As String
        Get
            Return strAbbreviation
        End Get
        Set(ByVal value As String)
            strAbbreviation = value
        End Set
    End Property
    
    Public Property Name() As String
        Get
            Return strName
        End Get
        Set(ByVal value As String)
            strName = value
        End Set
    End Property

    Public Function calcDensity() As Double
        Return intPopulation / intLandArea
    End Function

End Class
