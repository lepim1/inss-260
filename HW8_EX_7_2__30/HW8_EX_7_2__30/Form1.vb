﻿Public Class Form1

    Private Sub btEvaluateGrades_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btEvaluateGrades.Click
        Dim grades = IO.File.ReadAllLines("\\vmware-host\Shared Folders\Documents\Visual Studio 2010\Projects\Final.txt")
        Dim averageOrderCount = From grade As Double In grades
                                Select grade

        Dim customerOrderAverage = From grade As Double In grades
                                   Where grade > averageOrderCount.Average
                                   Select grade

        txtAverageGrade.Text = CStr(averageOrderCount.Average())

        txtPercAboveAvg.Text = CStr(customerOrderAverage.Count / averageOrderCount.Count)




    End Sub

End Class
