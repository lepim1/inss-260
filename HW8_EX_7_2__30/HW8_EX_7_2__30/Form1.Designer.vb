﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btEvaluateGrades = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtAverageGrade = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtPercAboveAvg = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'btEvaluateGrades
        '
        Me.btEvaluateGrades.Location = New System.Drawing.Point(58, 12)
        Me.btEvaluateGrades.Name = "btEvaluateGrades"
        Me.btEvaluateGrades.Size = New System.Drawing.Size(154, 23)
        Me.btEvaluateGrades.TabIndex = 0
        Me.btEvaluateGrades.Text = "Evaluate Grades"
        Me.btEvaluateGrades.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(31, 83)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Average grade:"
        '
        'txtAverageGrade
        '
        Me.txtAverageGrade.Enabled = False
        Me.txtAverageGrade.Location = New System.Drawing.Point(117, 80)
        Me.txtAverageGrade.Name = "txtAverageGrade"
        Me.txtAverageGrade.Size = New System.Drawing.Size(100, 20)
        Me.txtAverageGrade.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(31, 127)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(140, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Percentage above average:"
        '
        'txtPercAboveAvg
        '
        Me.txtPercAboveAvg.Enabled = False
        Me.txtPercAboveAvg.Location = New System.Drawing.Point(172, 124)
        Me.txtPercAboveAvg.Name = "txtPercAboveAvg"
        Me.txtPercAboveAvg.Size = New System.Drawing.Size(100, 20)
        Me.txtPercAboveAvg.TabIndex = 4
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 174)
        Me.Controls.Add(Me.txtPercAboveAvg)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtAverageGrade)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btEvaluateGrades)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btEvaluateGrades As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtAverageGrade As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtPercAboveAvg As System.Windows.Forms.TextBox

End Class
