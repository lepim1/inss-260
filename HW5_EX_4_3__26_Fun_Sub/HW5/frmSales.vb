﻿Public Class frmSales

    Private Sub btCalc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btCalc.Click
        Dim SP As Double
        Dim amount As Double
        Dim revenue As Double
        Dim overhead As Double
        Dim VC As Double
        Dim TC As Double
        Dim result As Double

        Try
            input(SP, amount, overhead, VC)
            revenue = computeRevenue(SP, amount)
            TC = computeTotalCost(overhead, VC, amount)
            result = computeProfitLoss(revenue, TC)
            output(revenue, TC, result)
        Catch ex As Exception

            Dim empty = Me.Controls.OfType(Of TextBox)().Where(Function(txt) txt.Text.Length = 0)
            If empty.Any Then
                MessageBox.Show(String.Format("Please fill all enabled textboxes"))
            End If
        End Try

    End Sub

    Public Sub input(ByRef SP As Double, ByRef amount As Double, ByRef overhead As Double, ByRef VC As Double)
        Try
            SP = CDbl(txtBoxSP.Text)
            amount = CDbl(txtBoxAS.Text)
            overhead = CDbl(txtBoxOver.Text)
            VC = CDbl(txtBoxVC.Text)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function computeRevenue(ByVal SP As Double, ByVal amount As Double) As Double
        Return SP * amount
    End Function

    Public Function computeTotalCost(ByVal overhead As Double, ByVal VC As Double, ByVal amount As Double) As Double
        Return overhead + VC * amount
    End Function

    Public Function computeProfitLoss(ByVal revenue As Double, ByVal TC As Double) As Double
        Return revenue - TC
    End Function

    Public Sub output(ByVal revenue As Double, ByVal TC As Double, ByVal result As Double)
        txtBoxRevenue.Text = CStr(revenue)
        txtBoxTC.Text = CStr(TC)
        txtBoxResult.Text = CStr(result)

        If result > 0 Then
            lblSmile.Text = "J"
        ElseIf result = 0 Then
            lblSmile.Text = "K"
        Else
            lblSmile.Text = "L"
        End If
    End Sub
End Class
