﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSales
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtBoxSP = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtBoxAS = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtBoxOver = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtBoxVC = New System.Windows.Forms.TextBox()
        Me.txtBoxRevenue = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtBoxTC = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtBoxResult = New System.Windows.Forms.TextBox()
        Me.btCalc = New System.Windows.Forms.Button()
        Me.lblSmile = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(39, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(125, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Selling price per unit (SP)"
        '
        'txtBoxSP
        '
        Me.txtBoxSP.Location = New System.Drawing.Point(177, 35)
        Me.txtBoxSP.Name = "txtBoxSP"
        Me.txtBoxSP.Size = New System.Drawing.Size(134, 20)
        Me.txtBoxSP.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(39, 63)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Amount sold (AS)"
        '
        'txtBoxAS
        '
        Me.txtBoxAS.Location = New System.Drawing.Point(177, 61)
        Me.txtBoxAS.Name = "txtBoxAS"
        Me.txtBoxAS.Size = New System.Drawing.Size(134, 20)
        Me.txtBoxAS.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(393, 38)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Overhead (O)"
        '
        'txtBoxOver
        '
        Me.txtBoxOver.Location = New System.Drawing.Point(531, 35)
        Me.txtBoxOver.Name = "txtBoxOver"
        Me.txtBoxOver.Size = New System.Drawing.Size(134, 20)
        Me.txtBoxOver.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(39, 90)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(99, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Revenue (SP x AS)"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(393, 63)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(130, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Variable Cost per unit (VC)"
        '
        'txtBoxVC
        '
        Me.txtBoxVC.Location = New System.Drawing.Point(531, 60)
        Me.txtBoxVC.Name = "txtBoxVC"
        Me.txtBoxVC.Size = New System.Drawing.Size(134, 20)
        Me.txtBoxVC.TabIndex = 8
        '
        'txtBoxRevenue
        '
        Me.txtBoxRevenue.Enabled = False
        Me.txtBoxRevenue.Location = New System.Drawing.Point(177, 87)
        Me.txtBoxRevenue.Name = "txtBoxRevenue"
        Me.txtBoxRevenue.Size = New System.Drawing.Size(134, 20)
        Me.txtBoxRevenue.TabIndex = 9
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(393, 90)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(123, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Total Cost (O + VC x AS)"
        '
        'txtBoxTC
        '
        Me.txtBoxTC.Enabled = False
        Me.txtBoxTC.Location = New System.Drawing.Point(531, 87)
        Me.txtBoxTC.Name = "txtBoxTC"
        Me.txtBoxTC.Size = New System.Drawing.Size(134, 20)
        Me.txtBoxTC.TabIndex = 11
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(164, 206)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(147, 13)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Result (Revenue - Total Cost)"
        '
        'txtBoxResult
        '
        Me.txtBoxResult.Enabled = False
        Me.txtBoxResult.Location = New System.Drawing.Point(317, 203)
        Me.txtBoxResult.Name = "txtBoxResult"
        Me.txtBoxResult.Size = New System.Drawing.Size(137, 20)
        Me.txtBoxResult.TabIndex = 13
        '
        'btCalc
        '
        Me.btCalc.Location = New System.Drawing.Point(304, 131)
        Me.btCalc.Name = "btCalc"
        Me.btCalc.Size = New System.Drawing.Size(75, 23)
        Me.btCalc.TabIndex = 14
        Me.btCalc.Text = "Calc"
        Me.btCalc.UseVisualStyleBackColor = True
        '
        'lblSmile
        '
        Me.lblSmile.AutoSize = True
        Me.lblSmile.Font = New System.Drawing.Font("Wingdings", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.lblSmile.Location = New System.Drawing.Point(301, 271)
        Me.lblSmile.Name = "lblSmile"
        Me.lblSmile.Size = New System.Drawing.Size(0, 53)
        Me.lblSmile.TabIndex = 15
        '
        'frmSales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(732, 542)
        Me.Controls.Add(Me.lblSmile)
        Me.Controls.Add(Me.btCalc)
        Me.Controls.Add(Me.txtBoxResult)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtBoxTC)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtBoxRevenue)
        Me.Controls.Add(Me.txtBoxVC)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtBoxOver)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtBoxAS)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtBoxSP)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmSales"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtBoxSP As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtBoxAS As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtBoxOver As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtBoxVC As System.Windows.Forms.TextBox
    Friend WithEvents txtBoxRevenue As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtBoxTC As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtBoxResult As System.Windows.Forms.TextBox
    Friend WithEvents btCalc As System.Windows.Forms.Button
    Friend WithEvents lblSmile As System.Windows.Forms.Label

End Class
