﻿Public Class Form1
    Private strStates() As String
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        strStates = IO.File.ReadAllLines("\\vmware-host\Shared Folders\Documents\Visual Studio 2010\Projects\States.txt")
        For Each state As String In strStates
            lstBoxStates.Items.Add(state)
        Next
    End Sub

    Private Sub btFirstSevenCharState_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btFirstSevenCharState.Click
        For Each state As String In strStates
            If state.Length = 7 Then
                txtFirstSevenCharState.Text = state
                Exit For
            End If
        Next

        'or
        'Dim states = From state In strStates
        'Where(state.Length = 7)
        '            Select state
        'txtFirstSevenCharState.Text = states(0)
    End Sub
End Class
