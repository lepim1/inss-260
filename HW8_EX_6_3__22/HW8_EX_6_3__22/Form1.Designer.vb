﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lstBoxStates = New System.Windows.Forms.ListBox()
        Me.btFirstSevenCharState = New System.Windows.Forms.Button()
        Me.txtFirstSevenCharState = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'lstBoxStates
        '
        Me.lstBoxStates.FormattingEnabled = True
        Me.lstBoxStates.Location = New System.Drawing.Point(12, 12)
        Me.lstBoxStates.Name = "lstBoxStates"
        Me.lstBoxStates.Size = New System.Drawing.Size(120, 134)
        Me.lstBoxStates.TabIndex = 0
        '
        'btFirstSevenCharState
        '
        Me.btFirstSevenCharState.Location = New System.Drawing.Point(138, 12)
        Me.btFirstSevenCharState.Name = "btFirstSevenCharState"
        Me.btFirstSevenCharState.Size = New System.Drawing.Size(134, 103)
        Me.btFirstSevenCharState.TabIndex = 1
        Me.btFirstSevenCharState.Text = "Determine First State Name That Is Seven Characters Long"
        Me.btFirstSevenCharState.UseVisualStyleBackColor = True
        '
        'txtFirstSevenCharState
        '
        Me.txtFirstSevenCharState.Location = New System.Drawing.Point(138, 126)
        Me.txtFirstSevenCharState.Name = "txtFirstSevenCharState"
        Me.txtFirstSevenCharState.Size = New System.Drawing.Size(134, 20)
        Me.txtFirstSevenCharState.TabIndex = 2
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 154)
        Me.Controls.Add(Me.txtFirstSevenCharState)
        Me.Controls.Add(Me.btFirstSevenCharState)
        Me.Controls.Add(Me.lstBoxStates)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lstBoxStates As System.Windows.Forms.ListBox
    Friend WithEvents btFirstSevenCharState As System.Windows.Forms.Button
    Friend WithEvents txtFirstSevenCharState As System.Windows.Forms.TextBox

End Class
