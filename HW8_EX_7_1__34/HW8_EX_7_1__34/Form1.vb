﻿Public Class Form1

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim numbers() As Integer = {2, 5, 8, 9}
        MessageBox.Show(CStr(computeAvg(numbers)))
    End Sub

    Private Function computeAvg(ByVal numbers As Integer()) As Double
        Dim sum As Integer = 0
        For Each n In numbers
            sum += n
        Next
        Return sum / numbers.Length
    End Function

End Class
