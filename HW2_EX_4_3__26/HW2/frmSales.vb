﻿Public Class frmSales

    Private Sub btCalc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btCalc.Click
        Dim SP As Double
        Dim amount As Double
        Dim revenue As Double
        Dim overhead As Double
        Dim VC As Double
        Dim TC As Double
        Dim result As Double

        Try
            'lets calculate this
            SP = CDbl(txtBoxSP.Text)
            amount = CDbl(txtBoxAS.Text)
            overhead = CDbl(txtBoxOver.Text)
            VC = CDbl(txtBoxVC.Text)

            revenue = SP * amount
            txtBoxRevenue.Text = CStr(revenue)

            TC = overhead + VC * amount
            txtBoxTC.Text = CStr(TC)

            result = revenue - TC
            txtBoxResult.Text = CStr(result)
            If result > 0 Then
                lblSmile.Text = "J"
            ElseIf result = 0 Then
                lblSmile.Text = "K"
            Else
                lblSmile.Text = "L"
            End If
        Catch ex As Exception
            Dim empty = Me.Controls.OfType(Of TextBox)().Where(Function(txt) txt.Text.Length = 0)
            If empty.Any Then
                MessageBox.Show(String.Format("Please fill all enabled textboxes"))
            End If
        End Try



    End Sub

End Class
