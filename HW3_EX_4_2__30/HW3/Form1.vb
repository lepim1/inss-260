﻿Public Class Form1

    Private Sub btWithdraw_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btWithdraw.Click
        Dim withdraw, balance As Double
        Dim output As String = ""
        withdraw = CDbl(txtWithdraw.Text)
        balance = CDbl(txtBalance.Text)

        If withdraw > balance Then
            output = "Withdraw denied."
        Else
            balance = balance - withdraw
            If balance < 150 Then
                output = "Balance below $150."
            End If
        End If

        txtOutput.Text = "Current balance is " & balance & ". " & output
    End Sub

End Class
