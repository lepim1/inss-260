﻿Imports System.Globalization

Public Class Form1

    Private Sub btCalculate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btCalculate.Click
        Dim amount As Double
        Dim interest As Double
        Dim time As Double = 0

        Try
            amount = CDbl(txtAmount.Text.Replace(".", ","))
            interest = CDbl(txtInterest.Text.Replace(".", ","))

            Dim target As Double = amount * 2

            Do While amount < target
                amount = (1 + interest) * amount
                time += 1
            Loop

            txtDoublingTime.Text = CStr(time)
        Catch ex As Exception
            MessageBox.Show("Please enter only integer or decimal numbers")
        End Try

        ' Gets a NumberFormatInfo associated with the en-US culture. 
        'Dim nfi As NumberFormatInfo = New CultureInfo("en-US", False).NumberFormat
        'myDouble.ToString("N",nfi))
        'Dim decimalSeparator As String = Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator
        'txtDoublingTime.Text = decimalSeparator


    End Sub
End Class
