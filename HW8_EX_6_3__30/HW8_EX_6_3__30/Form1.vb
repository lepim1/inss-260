﻿Public Class Form1
    Private strStates() As String
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        strStates = IO.File.ReadAllLines("\\vmware-host\Shared Folders\Documents\Visual Studio 2010\Projects\States.txt")
        For Each state As String In strStates
            lstBoxStates.Items.Add(state)
        Next
    End Sub

    Private Sub btNumTwoWords_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btNumTwoWords.Click
        txtNumTwoWords.Text = CStr(calcNumTwoWords(lstBoxStates.Items))
        'or convert it to an Array of String
        'txtNumTwoWords.Text = CStr(calcNumTwoWords(lstBoxStates.Items.OfType(Of String)().ToArray))
    End Sub

    Private Function calcNumTwoWords(ByVal objectCollection As ListBox.ObjectCollection) As Object
        Dim numTwoWords As Integer = 0
        For Each Str As String In objectCollection
            If (Str.Split(" "c).Length = 2) Then
                numTwoWords += 1
            End If
        Next
        Return numTwoWords
    End Function



End Class
