﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lstBoxStates = New System.Windows.Forms.ListBox()
        Me.btNumTwoWords = New System.Windows.Forms.Button()
        Me.txtNumTwoWords = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'lstBoxStates
        '
        Me.lstBoxStates.FormattingEnabled = True
        Me.lstBoxStates.Location = New System.Drawing.Point(12, 12)
        Me.lstBoxStates.Name = "lstBoxStates"
        Me.lstBoxStates.Size = New System.Drawing.Size(120, 121)
        Me.lstBoxStates.TabIndex = 0
        '
        'btNumTwoWords
        '
        Me.btNumTwoWords.Location = New System.Drawing.Point(138, 12)
        Me.btNumTwoWords.Name = "btNumTwoWords"
        Me.btNumTwoWords.Size = New System.Drawing.Size(93, 95)
        Me.btNumTwoWords.TabIndex = 1
        Me.btNumTwoWords.Text = "Determine Number of States Having Two-Word Names"
        Me.btNumTwoWords.UseVisualStyleBackColor = True
        '
        'txtNumTwoWords
        '
        Me.txtNumTwoWords.Enabled = False
        Me.txtNumTwoWords.Location = New System.Drawing.Point(138, 113)
        Me.txtNumTwoWords.Name = "txtNumTwoWords"
        Me.txtNumTwoWords.Size = New System.Drawing.Size(93, 20)
        Me.txtNumTwoWords.TabIndex = 2
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(243, 136)
        Me.Controls.Add(Me.txtNumTwoWords)
        Me.Controls.Add(Me.btNumTwoWords)
        Me.Controls.Add(Me.lstBoxStates)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lstBoxStates As System.Windows.Forms.ListBox
    Friend WithEvents btNumTwoWords As System.Windows.Forms.Button
    Friend WithEvents txtNumTwoWords As System.Windows.Forms.TextBox

End Class
